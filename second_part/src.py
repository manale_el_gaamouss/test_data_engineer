from sys import maxsize

class MySet(set):
    def __init__(self, set1=None):
        self.set1 = set1

    def __add__(self, other):
        setadd = set(self.set1 + other.set1)
        return setadd

    def __sub__(self, other):
        setsub = set(self.set1) - set(other.set1)
        return setsub


def decorator_check_max_int(func):
    def decorator_check(a, b):
        if a + b > maxsize:
            return maxsize
        else:
            return func(a, b)

    return decorator_check


@decorator_check_max_int
def add(a, b):
    return a + b


def ignore_exception(exception):
    return lambda x,y : None if (y == 0 or not(isinstance(y, int)) or not(isinstance(x, int))) else exception(x, y)



@ignore_exception
def div(a, b):
    return a / b


@ignore_exception
def raise_something(exception):
    raise exception


# exercise 4
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap


class MetaInherList(type):
    def __new__(self, class_name, class_bases, class_attrs):
        return type(class_name, class_bases, class_attrs)


class Ex:
    x = 4


class ForceToList(Ex, metaclass=MetaInherList):
    pass

