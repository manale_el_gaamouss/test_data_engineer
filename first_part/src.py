def exercise_one():
    for x in range(1, 100):
        if (x % 15 == 0):
            print("threefive")
        elif (x % 3 == 0):
            print("three")
        elif (x % 5 == 0):
            print("five")
        else:
            print(x)


def exercise_two(arr):
    sum_elements = (len(arr) + 1) * ((len(arr) + 1) + 1) / 2
    return sum_elements - sum(arr)

def exercise_three(arr):

 not_negative = []
 for i in range(len(arr)):
    if (arr[i] >= 0):
        not_negative.append(arr[i])

 not_negative = sorted(not_negative)

 j = 0
 for i in range(len(arr)):
    if (arr[i] >= 0):
        arr[i] = not_negative[j]
        j += 1

 for i in range(len(arr)):
    print(arr[i], end=" ")

