import pandas as pd
import numpy as np
from pathlib import Path

def exercice_one():

    #question1
    product_list = pd.read_json("data/products.json.gz")

    #question2
    def clean_cat(mycat):
        for i in mycat:
            mylist = i.split(',')
            newlist = [x for x in mylist if x != 'NULL']
            print(newlist)

    #question3
    df_clean_cat = clean_cat(product_list["idappcat"])


def import_raw_data(filepath, sep):
    return pd.read_csv(filepath, sep, comment='#', na_values=['Nothing'])


def process_data(data_shop, asset_data_rep):
    pe_ref_in_enseigne_values_correction = asset_data_rep.applymap(lambda x: str(x).replace("--", ""))

    cast_pe_ref_in_enseigne_values = pe_ref_in_enseigne_values_correction["pe_ref_in_enseigne"].apply(
        np.int64).reset_index()

    merge_by_pre_ref_in_enseigne = pe_ref_in_enseigne_values_correction.drop(["pe_ref_in_enseigne"], axis=1).merge(
        cast_pe_ref_in_enseigne_values, left_index=True, right_index=True).drop(["index"], axis=1)

    merge_data_shop_with_asset_data = merge_by_pre_ref_in_enseigne.merge(data_shop,
                                                                         left_on="pe_ref_in_enseigne",
                                                                         right_on="identifiantproduit", how='right')
    return merge_data_shop_with_asset_data


def average_prices(data_shop1, data_shop2):
    merge_data_shops = pd.concat([data_shop1, data_shop2], axis=0)
    average_prices = merge_data_shops.groupby('identifiantproduit').agg(averageprice=('prixproduit', 'mean'))
    filepath = Path('data/out.csv')
    filepath.parent.mkdir(parents=True, exist_ok=True)
    average_prices.to_csv(filepath)


def count_products_by_categories_by_day(merge_data_shop_with_asset_data):
    return merge_data_shop_with_asset_data.groupby(['p_id_cat', 'categorieenseigne']) \
        .size()


def average_products_by_categories(merge_data_shop_with_asset_data1, merge_data_shop_with_asset_data2):
    data = pd.concat([merge_data_shop_with_asset_data1, merge_data_shop_with_asset_data2], axis=0)

    return data.groupby(["p_id_cat", "categorieenseigne"]).agg({'pe_ref_in_enseigne': 'mean'})


if __name__ == '__main__':
    raw_data_20181017 = import_raw_data("data/17-10-2018.3880.gz", ";")
    raw_data_20181018 = import_raw_data("data/18-10-2018.3880.gz", ";")
    raw_data_asset_rep = import_raw_data("data/back_office.csv.gz", ",")

    processed_data_20181017 = process_data(raw_data_20181017, raw_data_asset_rep)
    processed_data_20181018 = process_data(raw_data_20181018, raw_data_asset_rep)

    avg_prices = average_prices(raw_data_20181017, raw_data_20181018)

    count_by_prod_cat_day_17 = count_products_by_categories_by_day(processed_data_20181017)
    count_by_prod_cat_day_18 = count_products_by_categories_by_day(processed_data_20181018)
